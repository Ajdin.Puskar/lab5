package cellular;

import java.util.Random;

import cellular.cellstate.CellState;
import cellular.cellstate.ICellState;
import datastructure.Grid;
import datastructure.GridDirection;
import datastructure.IGrid;
import datastructure.Location;

/**
 * An ICellAutomata that implements the Seeds Cellular Automaton.
 *
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @see
 *      <p>
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If a dead cell has exactly two alive| neighbors then it becomes alive,
 *      otherwise it dies.
 */
public class SeedsAutomaton implements ICellAutomaton {

	/**
	 * The grid containing the current generation.
	 */
	Grid currentGeneration;

	/**
	 * Construct a Seeds ICellAutomaton using a grid with the given height and
	 * width.
	 *
	 */
	public SeedsAutomaton(int rows, int columns) {
		currentGeneration = new Grid(rows, columns,
				CellState.DEAD);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (Object loc : currentGeneration.locations()) {
			if (random.nextBoolean()) {
				currentGeneration.set((Location) loc, CellState.ALIVE);
			} else {
				currentGeneration.set((Location) loc, CellState.DEAD);
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public ICellState getCellState(Location loc) {
		return (ICellState) currentGeneration.get(loc);
	}

	@Override
	public void step() {

		IGrid<ICellState> nextGeneration = new Grid<>(
				currentGeneration.numRows(), currentGeneration.numColumns(),
				CellState.ALIVE);

		for (Object loc : currentGeneration.locations()) {
			int numNeighbours = countNeighbours((Location) loc);
			if (numNeighbours == 2) {
				nextGeneration.set((Location) loc, CellState.ALIVE);
			} else {
				nextGeneration.set((Location) loc, CellState.DEAD);
			}
		}

		currentGeneration = (Grid) nextGeneration;
	}

	private int countNeighbours(Location loc) {
		int numNeighbors = 0;
		for (GridDirection dir : GridDirection.values()) {
			Location neighbor = loc.getNeighbor(dir);

			if (currentGeneration.isOnGrid(neighbor)) {
				try {
					if (currentGeneration.get(neighbor) == CellState.ALIVE) {
						numNeighbors++;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return numNeighbors;
	}

	@Override
	public Iterable<Location> locations() {
		return currentGeneration.locations();
	}
}
